---
comments: false
title: Download
menu: sidebar
weight: 10
---

**WARNING**: The information below are for test usage, we're still working with Chef to ensure those packages fit their
trademark policy.

[Our packages are here](http://downloads.cinc.sh)

You can programmatically install them using https://omnitruck.cinc.sh/install.sh as you would do for an [omnibus install
of Chef](https://docs.chef.io/install_omnibus.html)
