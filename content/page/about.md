---
comments: false
title: About
menu: sidebar
weight: 15
---

## What is Cinc ?
CINC is a recursive acronym for **C**INC **I**s **N**ot **C**hef

## Ok, fun, but do you have details ?
Yes, we're a small group of people trusting a free distribution of Chef is necessary along the commercial one made by
Chef.  The [trademark policy](https://www.chef.io/trademark-policy/) of Chef prevent the use of similarly named binaries
and the word Chef in outputs.  So we worked on creating a build of Chef free of trademarks, we did need a name and Cinc
got a fair reception from start and in [later opinion from the
community](https://discourse.chef.io/t/the-community-distribution-needs-a-name/15275)

## Got it, but who are you?
You can see the team in gitlab's [Cinc project team tab](https://gitlab.com/groups/cinc-project/-/group_members)

## How do you build on all those platforms?
[Ramereth (Lance Albertson)](https://gitlab.com/ramereth) is the director of the Oregon State University Open Source Lab
([OSUOSL](https://osuosl.org)) which provides the gitlab workers for Linux, Windows and Mac OS builds.
