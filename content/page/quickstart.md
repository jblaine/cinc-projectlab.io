---
comments: false
title: QuickStart
menu: sidebar
weight: 20
---

## How to start using cinc

To use the cinc family, things are pretty close of Chef. If you don't know Chef, don't be afraid this doc won't forget
you.

### What is the family?
- Cinc Client is the community version of Chef Infra Client
- Cinc Auditor is the community version of Chef Inspec
- [Biome](https://biome.sh) is the community version of Chef Habitat

Side note: biome is done by another team from the [habitat community slack](http://slack.habitat.sh/) in #community-distros channel and not directly linked to this team work.  
We'll have to work together to create biome packages of Cinc Client and Auditor.

### Learning the ropes

Here the best resource is starting with https://learn.chef.io and https://docs.chef.io as support, Cinc packages are
community builds of Chef products, so the documentation still apply.

You'll have to replace `chef-client` with `cinc-client`, `inspec` with `chef-auditor` and `hab` with `bio`

Small caveat: Cinc versions don't have yet a biome package, so stick with omnibus install of the programs for now.

### Download the package

All packages are published at http://downloads.cinc.sh

Walk your way toward the package you want, download and install as you'll do for another package on your system of
choice.

### Use the install script

Alternatively you can use the install script method described in [Chef's doc](https://docs.chef.io/install_omnibus.html)
using https://docs.chef.io/install_omnibus.html instead

To install Cinc Client with the latest version of 15 on Ubuntu for example:
``` console
curl -L https://omnitruck.cinc.sh/install.sh | sudo bash -s -- -v 15
```

For a windows box:

``` powershell
. { iwr -useb https://omnitruck.chef.io/install.ps1 } | iex; install -version 15
```

### Configuration

Cinc Client get its conf from `/etc/cinc/client.rb` on linux and `c:\cinc\client.rb` by default.

If you're moving from chef you can either use `cinc-client -c /etc/chef/client.rb` or replace cinc default config with
your chef one `cp /etc/chef/* /etc/cinc/`.

Advanced migration path may need more work to rename/copy/suppress one of the dir and create a symbolic link between the
two directories.

## You're Good to Go :)

If you have questions, feel free to join us in the #community-distros channel on Chef's [community
slack](http://community-slack.chef.io/)
