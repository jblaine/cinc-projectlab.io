---
title: Home
menu: sidebar
weight: 1
---

Welcome to the Cinc Team website, the menu on the left gives you the main entries.

You can find us in Chef's [community slack](http://community-slack.chef.io/) channel #community-distros

Have fun cooking with Cinc :)